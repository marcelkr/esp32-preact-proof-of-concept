export default (config, env, helpers) => {
    config.output.filename = '[name].js'
    config.output.chunkFilename = '[name].chunk.js'

    let { plugin } = helpers.getPluginsByName(config, 'ExtractTextPlugin')[0];
    plugin.filename = 'style.css'
}