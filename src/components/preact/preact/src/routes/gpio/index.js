import { h, Component } from 'preact';
import style from './style';

export default class Gpio extends Component {
	togglePin = () => {
		fetch('/gpio/toggle')
	}

	render () {
		return (
			<div class={style.gpio}>
				<h1>GPIO</h1>

				<button onClick={this.togglePin}>Toggle Pin</button>
			</div>
		)
	};
}
