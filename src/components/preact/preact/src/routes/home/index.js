import { h, Component } from 'preact';
import style from './style';

export default class Home extends Component {
	state = {
		version: {}
	}

	componentDidMount() {
		fetch('/version')
		.then((response) => response.json())
		.then((version) => this.setState({ version }))
	}

	render () {
		return (
			<div class={style.home}>
				<h1>PreactJS ESP32 PoC</h1>
				<p>This is a PreactJS ESP32 proof of concpet.</p>
				<p>The version information seen below is received via <code>fetch</code> when the page is loaded.</p>
				<table>
					<tr>
						<td>idf_version</td>
						<td>{ this.state.version.idf_version }</td>
					</tr>
					<tr>
						<td>chip_model</td>
						<td>{ this.state.version.chip_model }</td>
					</tr>
					<tr>
						<td>chip_features</td>
						<td>{ this.state.version.chip_features }</td>
					</tr>
					<tr>
						<td>chip_cores</td>
						<td>{ this.state.version.chip_cores }</td>
					</tr>
					<tr>
						<td>chip_revision</td>
						<td>{ this.state.version.chip_revision }</td>
					</tr>
					<tr>
						<td>app_desc version</td>
						<td>{ this.state.version.project_version }</td>
					</tr>
				</table>
			</div>
		)
	};
}
