idf_component_register(
    SRCS "httpd.c"
    INCLUDE_DIRS "include"
    REQUIRES esp_http_server
    PRIV_REQUIRES app_update
)

set(PREACT_SRSC
    preact/build/index.html
    preact/build/style.css
    preact/build/polyfills.js
    preact/build/bundle.js
    preact/build/route-home.chunk.js
    preact/build/route-gpio.chunk.js
)
set(PREACT_HDR "${CMAKE_CURRENT_SOURCE_DIR}/preact.h")
set(GZIPPED_FILES "")
set(HEADER_FILES "")

foreach(SRC_FILE ${PREACT_SRSC})
    get_filename_component(SRC_FILENAME ${SRC_FILE} NAME)
    set(GZIP_FILE ${SRC_FILENAME}.gz)
    add_custom_command(OUTPUT ${GZIP_FILE}
        COMMAND gzip --stdout ${CMAKE_CURRENT_SOURCE_DIR}/${SRC_FILE} > ${GZIP_FILE}
        DEPENDS ${SRC_FILE}
    )
    set(GZIPPED_FILES ${GZIP_FILE} ${GZIPPED_FILES})
endforeach()

foreach(GZIPPED_FILE ${GZIPPED_FILES})
    set(HEADER_FILE ${GZIPPED_FILE}.h)
    add_custom_command(OUTPUT ${HEADER_FILE}
        COMMAND xxd -i ${GZIPPED_FILE} > ${HEADER_FILE}
        DEPENDS ${GZIPPED_FILE}
        VERBATIM
    )
    set(HEADER_FILES ${HEADER_FILE} ${HEADER_FILES})
endforeach()

add_custom_command(OUTPUT ${PREACT_HDR}
    COMMAND ${CMAKE_COMMAND} -E echo "#pragma once" > ${PREACT_HDR}
    COMMAND cat ${HEADER_FILES} >> ${PREACT_HDR}

    COMMAND sed -i "s/unsigned char/const char/" ${PREACT_HDR}
    COMMAND sed -i "s/unsigned int/ssize_t/" ${PREACT_HDR}

    DEPENDS ${HEADER_FILES}
    VERBATIM
)

add_custom_target(preact_hdr DEPENDS ${PREACT_HDR})
add_dependencies(${COMPONENT_LIB} preact_hdr)
