#include <sys/param.h>
#include <esp_log.h>
#include <esp_err.h>
#include <esp_http_server.h>
#include <esp_system.h>
#include <esp_idf_version.h>
#include <esp_ota_ops.h>
#include <esp_system.h>
#include <driver/gpio.h>

#include "preact.h"

static const char *TAG = "HTTPD";

esp_err_t get_gpio_toggle(httpd_req_t *req)
{
    int level = gpio_get_level(GPIO_NUM_21);
    gpio_set_level(GPIO_NUM_21, (level == 0 ? 1 : 0));
    ESP_LOGI(TAG, "Toggle button pressed");
    httpd_resp_send(req, "", 0);
    return ESP_OK;
}

esp_err_t get_version_info(httpd_req_t *req)
{
    char buf[200];

    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);

    const esp_app_desc_t* app_desc = esp_ota_get_app_description();

    sprintf(buf, "{\"idf_version\":\"%i.%i.%i\",\"chip_model\":\"%u\",\"chip_features\":\"%u\",\"chip_cores\":\"%u\",\"chip_revision\":\"%u\",\"project_version\":\"%s\"}",
            ESP_IDF_VERSION_MAJOR,
            ESP_IDF_VERSION_MINOR,
            ESP_IDF_VERSION_PATCH,
            chip_info.model,
            chip_info.features,
            chip_info.cores,
            chip_info.revision,
            app_desc->version
    );

    httpd_resp_set_type(req, "application/json");
    httpd_resp_send(req, buf, strlen(buf));
    return ESP_OK;
}
esp_err_t get_preact_index(httpd_req_t *req)
{
    httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    httpd_resp_send(req, index_html_gz, index_html_gz_len);
    return ESP_OK;
}
esp_err_t get_preact_style_css(httpd_req_t *req)
{
    httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    httpd_resp_set_type(req, "text/css");
    httpd_resp_send(req, style_css_gz, style_css_gz_len);
    return ESP_OK;
}
esp_err_t get_uri_bundle_js(httpd_req_t *req)
{
    httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    httpd_resp_send(req, bundle_js_gz, bundle_js_gz_len);
    return ESP_OK;
}
esp_err_t get_uri_polyfill_js(httpd_req_t *req)
{
    httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    httpd_resp_send(req, polyfills_js_gz, polyfills_js_gz_len);
    return ESP_OK;
}
esp_err_t get_uri_route_home_js(httpd_req_t *req)
{
    httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    httpd_resp_send(req, route_home_chunk_js_gz, route_home_chunk_js_gz_len);
    return ESP_OK;
}
esp_err_t get_uri_route_gpio_js(httpd_req_t *req)
{
    httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
    httpd_resp_send(req, route_gpio_chunk_js_gz, route_gpio_chunk_js_gz_len);
    return ESP_OK;
}

httpd_uri_t uri_version = {
    .uri      = "/version",
    .method   = HTTP_GET,
    .handler  = get_version_info,
    .user_ctx = NULL
};
httpd_uri_t uri_preact_index = {
    .uri      = "/",
    .method   = HTTP_GET,
    .handler  = get_preact_index,
    .user_ctx = NULL
};
httpd_uri_t uri_preact_style_css = {
    .uri      = "/style.css",
    .method   = HTTP_GET,
    .handler  = get_preact_style_css,
    .user_ctx = NULL
};
httpd_uri_t uri_bundle_js = {
    .uri      = "/bundle.js",
    .method   = HTTP_GET,
    .handler  = get_uri_bundle_js,
    .user_ctx = NULL
};
httpd_uri_t uri_polyfill_js = {
    .uri      = "/polyfills.js",
    .method   = HTTP_GET,
    .handler  = get_uri_polyfill_js,
    .user_ctx = NULL
};
httpd_uri_t uri_route_home_js = {
    .uri      = "/route-home.chunk.js",
    .method   = HTTP_GET,
    .handler  = get_uri_route_home_js,
    .user_ctx = NULL
};
httpd_uri_t uri_route_gpio_js = {
    .uri      = "/route-gpio.chunk.js",
    .method   = HTTP_GET,
    .handler  = get_uri_route_gpio_js,
    .user_ctx = NULL
};
httpd_uri_t uri_gpio_toggle = {
    .uri      = "/gpio/toggle",
    .method   = HTTP_GET,
    .handler  = get_gpio_toggle,
    .user_ctx = NULL
};

httpd_handle_t start_webserver(void)
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_INPUT_OUTPUT;
    io_conf.pin_bit_mask = (1ULL<<GPIO_NUM_21);
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);


    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    /* Empty handle to esp_http_server */
    httpd_handle_t server = NULL;

    if (httpd_start(&server, &config) == ESP_OK) {
        httpd_register_uri_handler(server, &uri_preact_index);
        httpd_register_uri_handler(server, &uri_preact_style_css);
        httpd_register_uri_handler(server, &uri_bundle_js);
        httpd_register_uri_handler(server, &uri_polyfill_js);
        httpd_register_uri_handler(server, &uri_version);
        httpd_register_uri_handler(server, &uri_route_home_js);
        httpd_register_uri_handler(server, &uri_route_gpio_js);
        httpd_register_uri_handler(server, &uri_gpio_toggle);
    }
    /* If server failed to start, handle will be NULL */
    return server;
}

void stop_webserver(httpd_handle_t server)
{
    if (server) {
        httpd_stop(server);
    }
}