# ESP32 PreactJS Proof of Concept

![](doc/title.jpg)

This is a proof of concept of serving a [PreactJS](https://preactjs.com) application from an ESP32 controller and serving/querying a very basic JSON api. [Watch a video](https://youtu.be/hEHPkkr6Eus).

This PoC shows how to adapt the Preact config to play more nicely with the CMake-based Espressif toolchain. Furthermore there's a sample CMakeLists.txt to gzip and then bundle all the Preact file into a single C header file. Functionality-wise – apart from serving the app itself – it serves an API to query version information from the ESP and toggle a LED on GPIO 21.

The size of the gzipped preact sources in this example is 11867
 byte. The size of the whole preact component as shown via `idf.py size-components` is 13022 bytes.

## Prerequisites

The following software or hardware is used for this PoC:

- ESP32-WROOM-32
- LED with resistor
- Espressif IoT Development Framework (ESP-IDF) 4.0.0
- Linux (The components makefile is using some command line tools native to Linux. See [here](#cmake).)

## How to use or adapt it

1. Edit the preact files in `components/preact/preact` to your liking (a.k.a. "Create your own app").
2. Build the preact files. Executing `preact build` in `components/preact/preact`
3. Edit `components/preact/CMakeLists.txt`. Make sure all files you want to serve are present in the list `PREACT_SRSC`.
4. Make sure that all files you want to be served are present in `components/preact/httpd.c`.
5. `menuconfig` your project. In particular, insert your WiFi credentials. Use `idf.py menuconfig` and insert them in *Example Configuration*.
6. Build and flash the project using the ESP toolchain (You know, `idf.py build` and so on).
7. Monitor the ESP while it starts up (`idf.py monitor`) to find out the IP of the ESP. See the picture below.
8. Enter the IP in your browser.

![](doc/monitor_showing_ip.png)

## Under the hood

### Preact configuration

Preact (or rather Webpack) puts hash strings into the file names it outputs. E.g. `bundle.28a16f.js`. To play nicely with CMake this feature was disabled for all files being built by the `preact build` command. This makes it easier to pick up all the files in CMake and furthermore the URIs stay the same (which means, you don't have to adapt them in the C code all the time). All this is done in `components/preact/preact/preact.config.js`.

### CMake

To save precious ROM, the preact source files are stored gzipped on the ESP. The process of gzipping the sources, converting the source files into C arrays (using `xxd`) and finally putting them into a single C header file is done using CMake. See the picture below for an illustration of the 'build flow' exemplary for two files.

![](doc/build.svg)

To achive this, the Linux command line tools `gzip`, `xxd` and `cat` are used. See `components/preact/CMakeLists.txt` for details.

## Possible improvements

Since this is just a PoC there's obviously room for improvement.

- The `components/preact/httpd.c` could be split up into several files. You could for instance create a file serving the static Preact files and another serving the JSON API.
- Big parts of `components/preact/httpd.c` could be auto generated
- The site could be served via HTTPS
- Implement `favicon.ico` support and other bells and whistles